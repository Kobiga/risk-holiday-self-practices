import java.util.Scanner;
public class SpeedingFine {
	

		public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Driver Driving Speed = ");
			int Speed = scan.nextInt();
			
			System.out.print("Road limited speed = ");
			int limit = scan.nextInt();
			
			int OverSpeed=0;
			
			OverSpeed= Speed-limit;
			
			int fine = OverSpeed*20;
			
			
			System.out.println("The fine for driving at "+ Speed +" mph on a "+ limit +" mph road is " + fine +" dollars.");
		}
	
}
