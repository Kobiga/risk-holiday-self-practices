import java.util.Scanner;
/**
 * This class used calculate the capacity of water tank
 * @author Kobiga
 * @category jar file creation
 * @version 1.0
 * @since 20 may 2020
 *
 */
public class CalculateCapacity {
	/**
	 * to performed enter the  radius and height of tank
	 * Calculate the capacity of the water tank
	 * @param args  the command line argument
	 * @param H, is height of tank
	 * @param R, is radius of tank base.
	 * @return result 
	 */
	static double H;
	static double R;
	public static void main(String[] args) {
		/**
		 * used two method one is main and other hand is format method
		 */
		Format(H,R);
		
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter height: ");
		double H=sc.nextDouble();
		
		System.out.println("Enter radius: ");
		double R=sc.nextDouble();
		sc.close();
		System.out.println("Capacity of Water tank:" +Format( H,  R)+ "cm*cm");
		
	}
	static double Format(double H,double R) {
		return (22/7)*R*R*H;
		
		
	}

}
