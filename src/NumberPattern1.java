import java.util.Scanner;
public class NumberPattern1 {
	public static void main (String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter num: ");
		int n=scan.nextInt();
	
		for (int i = 0; i<=n; i+=2) {
			for(int j = i + 1; j<=n; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
}
