
public class NumeralSum {
	public static int sum(String str) {
		int sum=0;
		String te="";
		
		for(int i = 0; i < str.length(); i++)  
        {  
            char ch = str.charAt(i); 
            if (Character.isDigit(ch))  
                te += ch;  
       
            else
            {   
                sum += Integer.parseInt(te);  
      
                
                te = "0";  //reset te
            }  
        } 
		return sum + Integer.parseInt(te);  
    }  
      
     
    public static void main (String[] args) 
    { 
          
        
        String str = "83ana24";  
      
        System.out.println(sum(str)); 
    } 

  
              
	}


