import java.util.Scanner;

public class Diamond {
	
	public static void main(String args[]){
		
	int i,j;
	Scanner scan=new Scanner(System.in);
	System.out.print("Enter the number of rows: ");

	int rows=scan.nextInt();//get input 

	
	for(i=1; i<=rows; i++){
	    for(j=rows; j>i; j--){
	       System.out.print(" ");
	    }
	    System.out.print("/");  
	    for(j=0; j<(i-1)*2; j++){
	       System.out.print(" ");
	    }
	    if(i==0){
	      System.out.print("\n");
	    }
	    else{
	      System.out.print("\\\n");
	    }
	}
	    
	for(i=rows-1; i>=0; i--){
	    for(j=rows-1; j>i; j--){
	       System.out.print(" ");
	    }
	    System.out.print("\\");
	    for(j=0; j<(i)*2; j++){
	       System.out.print(" ");
	    }
	    if(i==-1){
	      System.out.print("\n");
	    }
	    else{
	      System.out.print("/\n");
	    }
	    }
	    }
	    }



